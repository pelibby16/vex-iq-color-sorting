# Vex IQ Color Sorting
Created for ECCS ESEM robotics project.

Version 1 Example Images:

![img1](img/PXL_20220927_151904004.jpg)
![img2](img/PXL_20220927_151909617.jpg)
![img3](img/PXL_20220927_151915276.jpg)
![img4](img/PXL_20220927_151919462.jpg)
![img5](img/PXL_20220927_151923585.jpg)
![img6](img/PXL_20220927_151926360.jpg)